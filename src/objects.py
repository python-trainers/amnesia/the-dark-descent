from trainerbase.gameobject import GameFloat, GameInt
from trainerbase.memory import Address

from memory import player_base_pointer


player_base_address = Address(player_base_pointer)

health = GameFloat(player_base_address.inherit(extra_offsets=[0x84]))
sanity = GameFloat(player_base_address.inherit(extra_offsets=[0x88]))
lantern_oil = GameFloat(player_base_address.inherit(extra_offsets=[0x8C]))
tinderbox = GameInt(player_base_address.inherit(extra_offsets=[0x98]))
