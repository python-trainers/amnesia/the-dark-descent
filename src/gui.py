from trainerbase.gui import add_gameobject_to_gui, simple_trainerbase_menu

from objects import health, lantern_oil, sanity, tinderbox


@simple_trainerbase_menu("The Dark Descent", 700, 200)
def run_menu():
    add_gameobject_to_gui(health, "Health", "F1", default_setter_input_value=100.0)
    add_gameobject_to_gui(sanity, "Sanity", "F2", default_setter_input_value=100.0)
    add_gameobject_to_gui(lantern_oil, "Lantern Oil", "F3", default_setter_input_value=100.0)
    add_gameobject_to_gui(tinderbox, "Tinderbox", "F4", default_setter_input_value=1000)
