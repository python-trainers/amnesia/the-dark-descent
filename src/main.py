from os import _exit as force_exit

from trainerbase.codeinjection import safely_eject_all_code_injections
from trainerbase.scriptengine import system_script_engine

from gui import run_menu
from injections import update_player_base_pointer


def on_initialized():
    system_script_engine.start()

    update_player_base_pointer.inject()


def on_shutdown():
    system_script_engine.stop()

    safely_eject_all_code_injections()


def main():
    run_menu(on_initialized=on_initialized)
    on_shutdown()
    force_exit(0)


if __name__ == "__main__":
    main()
