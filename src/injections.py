from trainerbase.codeinjection import AllocatingCodeInjection
from trainerbase.memory import pm

from memory import player_base_pointer


update_player_base_pointer = AllocatingCodeInjection(
    pm.base_address + 0x96E29,
    f"""
        mov [{player_base_pointer}], ecx
        mov eax, [ecx + 0x98]
    """,
    original_code_length=6,
)
